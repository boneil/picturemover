﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace PictureMover
{
    class Program
    {
        static string inputDir = @"C:\Users\boneil\Pictures";
        static string outputDir = @"c:\pics\out";

        static DirManager manager = new DirManager(outputDir);

        static void Main(string[] args)
        {
            Console.WriteLine("starting... ");
            ProcessDirectory(inputDir);

            Console.WriteLine("Done!");

            Console.ReadKey();
        }

        static void ProcessDirectory(string dir)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(dir);

            if (dirInfo.EnumerateDirectories().Count() > 0)
            {
                foreach (var subDir in dirInfo.EnumerateDirectories())
                {
                    ProcessDirectory(subDir.FullName);
                }
            }

            Console.WriteLine("Processing Directory: " + dir);

            foreach (var file in dirInfo.EnumerateFiles("*.jpg"))
            {
                manager.MoveFileIntoStructure(file.FullName, true);                
            }
        }
    }

    class DirManager
    {
        public static readonly string[] months = new string[] { "Janurary", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

        public static readonly string dirTemplate = "{0}_{1}";

        private static Regex r = new Regex(":");

        public string ManagedDir { get; set; }

        public DirManager(string directory)
        {
            this.ManagedDir = directory;
        }

        public void MoveFileIntoStructure(string fileName, bool copyOnly = false)
        {
            FileInfo fileInfo = new FileInfo(fileName);

            var takenDate = GetDateTakenFromImage(fileName); //fileInfo.LastWriteTime;

            //make sure the structure is ready for the file
            EnsureYearStructure(takenDate.Year);

            string newFilePath = this.PathForDate(takenDate);

            string newFileName = Path.Combine(newFilePath, Path.GetFileName(fileName));

            if(File.Exists(newFileName))
            {
                FileInfo existFile = new FileInfo(newFileName);

                if (fileInfo.LastWriteTime == existFile.LastWriteTime && fileInfo.Length == existFile.Length)
                {
                    Console.WriteLine("Same file found, skipping: " + fileName);

                    return;
                }
                else
                {
                    //looks like a different file with the same name (this is just a hack for now, don't want to keep using GUID for file conflicts)
                    newFileName = Path.Combine(Path.GetDirectoryName(newFileName), "----" + Guid.NewGuid().ToString() + Path.GetFileName(newFileName));
                }
                
            }
            
            File.Copy(fileName, newFileName);

            if (!copyOnly)
            {
                File.Delete(fileName);
            }
        }

        public void EnsureYearStructure(int year)
        {
            string yearPath = Path.Combine(ManagedDir, year.ToString());
            System.Diagnostics.Trace.WriteLine("checking year structure for: " + yearPath);
            if(Directory.Exists(yearPath))
            {
                Directory.CreateDirectory(yearPath);
            }

            //check each sub dir
            for (int i = 0; i < 12; i++)
            {
                string monthPath = Path.Combine(yearPath, string.Format(dirTemplate, i+1, months[i]));
                if (!Directory.Exists(monthPath))
                {
                    Directory.CreateDirectory(monthPath);
                }
            }
        }

        protected string PathForDate(DateTime date)
        {
            var destPath = Path.Combine(this.ManagedDir, date.Year.ToString());
            string pathForDate = Path.Combine(destPath, string.Format(dirTemplate, date.Month, months[date.Month - 1]));

            return pathForDate;
        }

        //retrieves the datetime WITHOUT loading the whole image
        public static DateTime GetDateTakenFromImage(string path)
        {
            try
            {
                using (FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read))
                using (Image myImage = Image.FromStream(fs, false, false))
                {
                    if (myImage.PropertyIdList.Any(x => x == 36867))
                    {
                        PropertyItem propItem = myImage.GetPropertyItem(36867);
                        string dateTaken = r.Replace(Encoding.UTF8.GetString(propItem.Value), "-", 2);
                        return DateTime.Parse(dateTaken);
                    }
                    else
                    {
                        FileInfo fileInfo = new FileInfo(path);
                        return fileInfo.LastWriteTime;
                    }
                }
            }
            catch (Exception err)
            {
                System.Diagnostics.Trace.TraceError("Failed to get date take for file: {0}.  Error: {1}", path, err.ToString());

                FileInfo fileInfo = new FileInfo(path);
                return fileInfo.LastWriteTime;
            }
        }
    }
}
